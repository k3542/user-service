export default class ApiResponse {
  private statusCode: number;
  private message: string;
  private data: any;

  constructor(statusCode: number, message: string, data: any = null) {
    this.statusCode = statusCode || 200;
    this.message = message || "OK";
    this.data = data || null;
  }
}
