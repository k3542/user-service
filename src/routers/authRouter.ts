import { Router, Request, Response } from "express";
import { CONFLICT, OK, INTERNAL_SERVER_ERROR, CREATED } from "http-status";
import AuthService from "../services/authService";
import ApiError from "../errors/apiError";
import ApiResponse from "../lib/apiResponse";

export const authRouter = Router();
const authService = new AuthService();

authRouter.post("/register", async (req: Request, res: Response) => {
  const { full_name, username, password } = req.body;

  const user = await authService.register({
    full_name,
    username,
    password,
  });

  if (user instanceof ApiError) {
    return res.status(CONFLICT).json(user);
  }

  return res
    .status(CREATED)
    .json(new ApiResponse(CREATED, "Successfuly create new user", user));
});

authRouter.post("/login", async (req: Request, res: Response) => {
  const { username, password } = req.body;

  const token = await authService.login({
    username,
    password,
  });

  if (token instanceof ApiError) {
    return res.status(INTERNAL_SERVER_ERROR).json(token);
  }

  return res.status(OK).json(new ApiResponse(OK, "Login successful", token));
});

authRouter.post("/verify", async (req: Request, res: Response) => {
  const { token } = req.body;

  const user = await authService.verify(token);

  if (user instanceof ApiError) {
    return res.status(INTERNAL_SERVER_ERROR).json(user);
  }

  return res.status(OK).json(new ApiResponse(OK, "Token verified", user));
});
