import { prisma } from "../lib/prisma";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import {
  noUsernameFoundError,
  usernameAlreadyExistError,
  wrongPasswordError,
  tokenError,
} from "../errors/serviceError";

export default class AuthService {
  async register(options: {
    full_name: string;
    username: string;
    password: string;
  }) {
    const hashedPassword = await bcrypt.hash(options.password, 10);

    let user;

    try {
      user = await prisma.user.create({
        data: {
          full_name: options.full_name,
          username: options.username,
          password: hashedPassword,
        },
      });
    } catch (error) {
      return usernameAlreadyExistError();
    }

    return user;
  }

  async login(options: { username: string; password: string }) {
    const user = await prisma.user.findUnique({
      where: { username: options.username },
    });

    if (!user) {
      return noUsernameFoundError();
    }

    const verified = await bcrypt.compare(options.password, user.password);

    if (!verified) {
      return wrongPasswordError();
    }

    const token = jwt.sign(user, "secret", {
      expiresIn: "3h",
    });

    return { token, user };
  }

  async verify(options: { token: string }) {
    let payload;

    try {
      payload = jwt.verify(options.token, "secret");
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        return tokenError();
      }
    }

    return { payload };
  }
}
